��    \      �     �      �     �     �     �     �     �  .   �     ,     4  &   @     g     |     �      �  :   �     �  
   �     	     	  
   	  
   '	     2	     >	  >  F	  
   �
     �
     �
     �
     �
     �
     �
     �
            	   .     8     @     I  	   R     \  	   l  ?   v     �     �     �     �     �     �           	            2        K  	   Y     c     l  3   t     �     �     �     �     �     �     �     �  
             &     8     H     N     V     ]     l     x     �  	   �     �     �     �     �     �     �  +        8     M     a  
   i     t  
   }     �  i  �     �                       7     	   V     `  %   p     �     �     �  (   �  8   �     /  	   6     @     F  
   T  
   _     j     v  �   {     p     |     �     �  '   �  
   �  -   �  '   �  )        H  
   \     g     o     w     �     �     �  V   �          	               5     >     V     d     h     n  8   t     �  
   �  
   �     �  9   �          $     <     K     X     a     r     x     �     �     �     �     �     �     �               !     7     ;     N     W     n     �     �     �  8   �          #     :     A     O     [  	   i                =      2      K                      0          P   
   %   :   3                 *      &              J   N   +   "         4              -                ;   \       A   (                         	       Q      [   6   X   9   !          F   .   C   O      $          M      D   V   W   '      1   H   @          I   8               #   Z   L       ?   G   B   >                   Y   5       )   E      /   R       ,           S   U      T         7   <        1 hr. 12 hrs. 2 hrs. 24 hrs. 6 hrs. A forecast application with OpenWeatherMap API API key About Meteo At least of 3 characters are required! Based in a mockup by Carlos Suarez Change location Choose your city with popup maps Choose your units from metric, imperial or british systems Close Cloudiness Clouds Collaborators Coord. lat Coord. lon Coordinates Country Current weather, with information about temperature, pressure, wind speed and wind direction, sunrise and sunset. Know meteorologic predictions to next hours and days. Show awesome maps with all the information. Switch between some maps distributors. Now with System Tray Indicator, showing your location and forecast. Dark theme Data Description Error Find my location automatically Forecast Forecast App for desktop Forecast for next 18 hours Forecast for next five days Found an error Full Moon General Humidity ID place Icon file Imperial System Interface Know the forecast of the next hours and days with data and maps Language Last Quarter Moon Lat Launch on start Loading Loading cache data Location Lon Maps Meteo Meteo;Weather;Forecast;Temperature;Wind;Snow;Rain; Metric System Moonphase New Moon No data No data received, loading cache. Are you connected? Options Other features: Precipitation Preferences Pressure Quarter Moon Quit Search for new location Show Meteo Some icons by Special Thanks to Start minimized State Sunrise Sunset Symbolic icons Temperature Terms of Service Today UK System Units Update conditions Update conditions every Use System Tray Indicator Waning Crescent Moon Waning Gibbous Moon Watch awesome maps with weather information Waxing Crescent Moon Waxing Gibbous Moon Weather Wind Speed Wind dir Wind speed website Project-Id-Version: weather
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-30 13:31+0200
Last-Translator: Carlos Suárez <bitseater@gmail.com>
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
 1 hr. 12 hrs. 2 hrs. 24 hrs. 6 hrs. Aplicación de predicción con la API de OpenWeatherMap Clave API Acerca de Meteo ¡Se necesitan al menos 3 caracteres! Basado en una idea de Carlos Suarez Cambiar ubicación Elija localidad desde mapas informativos Elija unidades (Sistema métrico, imperial o británico) Cerrar Nubosidad Nubes Colaboradores Coord. lat Coord. lon Coordenadas Pais Situación meteorológica actual, con información sobre temperatura, presión, velocidad y dirección del viento, amanecer y crepúsculo. Muestra divertidos mapas con toda la información. Se puede cambiar entre distintos proveedores de mapas. Tema oscuro Datos Descripción Error Encuentra mi posición automáticamente Previsión Aplicación de predicción para el escritorio Predicción para las próximas 18 horas Previsión para los próximos cinco días Encontrado un error Luna Llena General Humedad ID lugar Fichero de icono Sistema Imperial Interfaz Conozca la previsión meteorológica de las próximas horas y días, con datos y mapas Idioma Cuarto Menguante Lat Iniciar en el arranque Cargando Cargando cache de datos Localización Lon Mapas Meteo Meteo;Tiempo;Previsión;Temperatura;Viento;Nieve;Lluvia; Sistema Métrico Fase Lunar Luna Nueva No hay datos No se reciben datos, cargando caché. ¿Estás conectado? Opciones Otras características: Precipitación Preferencias Presión Cuarto Creciente Salir Buscar nueva ubicación Mostrar Meteo Algunos íconos por Gracias especialmente a Comenzar minimizado Estado/Región Amanecer Crepúsculo Iconos simbólicos Temperatura Términos de Servicio Hoy Sistema británico Unidades Actualizar condiciones Actualizar condiciones cada Usa indicador de sistema Luna Menguante cóncava Luna Menguante convexa Observe divertidos mapas con información meteorológica Luna Creciente cóncava Luna Creciente convexa Tiempo Veloc. viento Dir. viento Veloc. viento sitio web 